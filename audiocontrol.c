/*
	Audio control panel for DeskView. Uses ALSA/amixer and
	PulseAudio/pacmd commands on Linux.
*/

/*
	BSD Zero Clause License (0BSD)

	Permission to use, copy, modify, and/or distribute this
	software for any purpose with or without fee is hereby
	granted.

	THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS
	ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO
	EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
	INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
	WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
	TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
	THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <unistd.h> /* sleep() */
#include <stdio.h> /* standard I/O for printf(), etc */
#include <stdlib.h> /* system() to run commands */
#include <pthread.h> /* POSIX multi-threading */
#include <signal.h> /* POSIX signals */

#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h> /* top level shell */
#include <X11/xpm.h> /* X11 pixmaps */
#include <X11/Xutil.h>

#include <Xm/Xm.h> /* motif core */
#include <Xm/MwmUtil.h> /* manipulate mwm */
#include <Xm/MainW.h> /* high level manager widget */
#include <Xm/Form.h> /* manager form */
#include <Xm/Scale.h> /* volume slider */
#include <Xm/Frame.h> /* color bars */
#include <Xm/Label.h> /* master label */
#include <Xm/ToggleB.h> /* mute toggle */
#include <Xm/PushB.h> /* reset button */
#include <Xm/RowColumn.h> /* menu bar */
#include <Xm/CascadeB.h> /* cascading menus */
#include <Xm/Text.h> /* text widgets for popups */
#include <Xm/XmStrDefs.h> /* for fetching text dimensions */
#include <Xm/DialogS.h> /* popup dialogs for help/about */

/* set up global access to Xt display and app contexts */
Display *display;
XtAppContext app;

/* set up global access to some Motif widgets */
Widget  topLevel, 
	masterScale, masterLabel, 
	dbLabel, percLabel, 
	toggleButton;

/* random variables for the GUI */
const char *versionString = "1.0";
const char *dateString = "January 30th, 2025";

/* command strings - these should be edited if porting to non-linux operating systems */
const char *currentVolumeCmdStr = "pacmd list-sinks | grep \'front-left:\' | awk \'{print $5}\' | tr -d \"%\n\"";
const char *setMasterCmdStr = "amixer set Master %d%% > /dev/null 2>&1";
const char *muteMasterCmdStr = "amixer set Master mute > /dev/null 2>&1";
const char *unmuteMasterCmdStr = "amixer set Master unmute > /dev/null 2>&1";

int outputType = 0; /* 0 none, 1 speakers, 2 headphones */
int masterVolume = 40;
int scaleIsDragging = 0;

int setMasterOnStartup = 0;
int muteOnStartup = 0;

/* anything over 90ms for widgets feels sluggish */
long unsigned int widgetUpdate = 80;
long unsigned int outputUpdate = 800;

/* pthread for the main user interface */
static pthread_t motifGUI;

typedef struct
{
	int argc;
	char **argv;
	
} MotifThreadArgs;


#include "icon_data.h" /* xbm data for icons */
#include "info_dialog.h" /* custom popup dialogs */
#include "callbacks.h" /* callback functions */
#include "gui.h" /* motif widgets */


int main(int argc, char *argv[])
{
/* handle command line help flags */
	for (int i = 1; i < argc; i++)
	{
		/* string compare arguments for the most common help flags */
		if(strcmp(argv[i], "h") == 0 
		|| strcmp(argv[i], "-h") == 0
		|| strcmp(argv[i], "--h") == 0
		|| strcmp(argv[i], "help") == 0
		|| strcmp(argv[i], "-help") == 0 
		|| strcmp(argv[i], "--help") == 0)
		{
			printf("\033[7m%s\033[0m\n", "\n DeskView Audio Control Panel ");
			
			printf("\n Version: %s (%s)", versionString, dateString);
			
			/*printf("\033[7m%s\033[0m\n", "\n\n\n General Options: ");
			
			printf("\n -d -default  -> Sets master to 40 percent.");
			printf("\n -v -volume   -> Same as -d but with a custom value.");
			printf("\n -m -mute     -> Mute master output on startup.");*/
			
			printf("\033[7m%s\033[0m\n", "\n\n\n Window Management: ");
			
			printf("\n -g -geometry -> Sets initial window geometry.");
			printf("\n -i -iconic   -> Iconify window on startup.");
			
			printf("\033[7m%s\033[0m\n", "\n\n\n Signals: ");
			
			printf("\n SIGUSR1 -> Lower volume by 1 percent.");
			printf("\n SIGUSR2 -> Raise volume by 1 percent.");
			printf("\n SIGALRM -> Toggle mute state on/off.");

			printf("\033[7m%s\033[0m\n", "\n\n\n Examples: ");
			
			printf("\n ~$ dvaudio -g +10-10 --volume 25");
			printf("\n ~$ kill -s SIGALRM $(pidof dvaudio)\n\n");
			
			return 0;
		}
		
		/*
		if(strcmp(argv[i], "-m") == 0 
		|| strcmp(argv[i], "-mute") == 0)
		{
			printf("\n  mute flag...\n\n");
		}
		
		if(strcmp(argv[i], "-d") == 0 
		|| strcmp(argv[i], "-default") == 0)
		{
			printf("\n  def flag...\n\n");
		}
		
		if(strcmp(argv[i], "-v") == 0 
		|| strcmp(argv[i], "-volume") == 0)
		{
			printf("\n  vol flag...\n\n");
		}
		*/
		
		break;
		
		/*
		else
		{
			printf("\n  Bad command line argument.");
			printf("\n  Try -help for more info.\n\n");
			
			return 0;
		}
		*/
	}
	
/* init X11 multi-threading */
	XInitThreads();

/* handle IPC signals */
	struct sigaction usr1_action, usr2_action, alarm_action;
    
	/* clear signal sets */
	sigemptyset(&usr1_action.sa_mask);
	sigemptyset(&usr2_action.sa_mask);
	sigemptyset(&alarm_action.sa_mask);
	
	/* set up SIGUSR1/2 */
	usr1_action.sa_handler = userSignal1;
	usr1_action.sa_flags = 0;
	sigaction(SIGUSR1, &usr1_action, NULL);
	
	usr2_action.sa_handler = userSignal2;
	usr2_action.sa_flags = 0;
	sigaction(SIGUSR2, &usr2_action, NULL);
	
	/* POSIX designers forgot to nail this to the floor */
	alarm_action.sa_handler = userSignal3;
	alarm_action.sa_flags = 0;
	sigaction(SIGALRM, &alarm_action, NULL);
	
	
/* grab current volume level from system */
	masterVolume = fetchCurrentVolume();
	
/* pthread to create the main graphical window */
	createUserInterface(argc, argv);
	
/* wait 1 sec to make sure motif widgets have been created */
	sleep(1);
	
/* holds main thread open */
	while(1)
	{
		sleep(86400);
		NULL;
	}

/* never runs but makes compilers happy */
	return 0;
}