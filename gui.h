/* fallback Xresources */
static String fb_xres[] = 
{
	/* some optional font settings */
	"*renderTable: xft",
	"*xft*fontType: FONT_IS_XFT",
	"*xft*fontName: Roboto",
	"*xft*fontSize: 11",
	"*xft*autohint: 1",
	"*xft*lcdfilter: lcddefault",
	"*xft*hintstyle: hintslight",
	"*xft*hinting: True",
	"*xft*antialias: 1",
	"*xft*rgba: rgb",
	"*xft*dpi: 96",
	
	"*infoTextArea*fontStyle: Bold",
	"*masterLabel*fontStyle: Italic",
	
	NULL,
};



void createColorBars(Widget widget, int barHeight)
{
	Widget colorBarForm = XtVaCreateManagedWidget("colorBarForm", xmFormWidgetClass, widget,
		XmNshadowThickness, 0,
		XmNfractionBase, 100,
	NULL);
	
	Widget colorBarGreen = XtVaCreateManagedWidget("colorBarGreen", xmFrameWidgetClass, colorBarForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 41,
		XmNbottomAttachment, XmATTACH_FORM,
		
		XmNheight, barHeight,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#7fc693", 8,
		XtVaTypedArg, XmNbackground, XmRString, "#5b906a", 8,
		XtVaTypedArg, XmNbottomShadowColor, XmRString, "#3d6047", 8,
	NULL);
	
	Widget colorBarYellow = XtVaCreateManagedWidget("colorBarYellow", xmFrameWidgetClass, colorBarForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_POSITION,
		XmNleftPosition, 41,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 77,
		XmNbottomAttachment, XmATTACH_FORM,
		
		XmNheight, barHeight,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#d6cd0b", 8,
		XtVaTypedArg, XmNbackground, XmRString, "#cab117", 8,
		XtVaTypedArg, XmNbottomShadowColor, XmRString, "#7e750a", 8,
	NULL);
	
	Widget colorBarRed = XtVaCreateManagedWidget("colorBarRed", xmFrameWidgetClass, colorBarForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_POSITION,
		XmNleftPosition, 77,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 100,
		XmNbottomAttachment, XmATTACH_FORM,
		
		XmNheight, barHeight,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#dd6f6c", 8,
		XtVaTypedArg, XmNbackground, XmRString, "#b04744", 8,
		XtVaTypedArg, XmNbottomShadowColor, XmRString, "#642c2a", 8,
	NULL);
	
}



void *spawnMotifGUI(void *arg)
{
	MotifThreadArgs *args = (MotifThreadArgs *)arg;
	int argc = args->argc;
	char **argv = args->argv;
	
/* init X11 toolkit and application context */
	
/* setup localization */
	XtSetLanguageProc(NULL, NULL, NULL);
	
/* open top level shell with fallback resources */

	topLevel = XtVaOpenApplication(&app, argv[0], NULL, 0, &argc, argv, fb_xres, sessionShellWidgetClass,
		XmNtitle, "Audio Control",
		XmNiconName, "Audio", /* title for iconified/minimized window */
		
		XmNminWidth, 320,
		XmNmaxWidth, 320,
		XmNwidth, 320,
	NULL);

	int decor;
	XtVaGetValues(topLevel, XmNmwmDecorations, &decor, NULL);
		decor &= ~MWM_DECOR_MENU;
		decor &= ~MWM_DECOR_BORDER;
		decor &= ~MWM_DECOR_MINIMIZE;
	XtVaSetValues(topLevel, XmNmwmDecorations, decor, NULL);
	
	int func;
	XtVaGetValues(topLevel, XmNmwmFunctions, &func, NULL);
		func &= ~MWM_FUNC_CLOSE;
		func &= ~MWM_FUNC_MOVE;
		func &= ~MWM_FUNC_MINIMIZE;
	XtVaSetValues(topLevel, XmNmwmFunctions, func, NULL);
	

/* XmMainWindow as high level container widget */
	Widget mainWin = XtVaCreateManagedWidget("mainWin", xmMainWindowWidgetClass, topLevel, NULL);
	XtVaSetValues(mainWin,
		XmNshadowThickness, 0,
	NULL);

	
/* create a menu bar */
	Widget menuBar = XtVaCreateManagedWidget("menuBar", xmRowColumnWidgetClass, mainWin, 
		XmNrowColumnType, XmMENU_BAR,
		XmNshadowThickness, 1,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNmarginHeight, 4,
	NULL);
	
	
/* create a manager form */
	Widget mainForm = XmCreateForm(mainWin, "mainForm", NULL, 0);
	XtVaSetValues(mainForm,
		/* 1px top and bottom shadows next to wm borders */
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_IN,
		
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, menuBar,
	NULL);
	
	/* grab colors for later use with widgets */
	Pixel menu_bg, menu_fg, menu_text;
	XtVaGetValues(menuBar, XmNbackground, &menu_bg, NULL);
	XtVaGetValues(mainForm, XmNforeground, &menu_fg, NULL);
	XtVaGetValues(mainForm, XmNbackground, &menu_text, NULL);
	
	Pixel main_ts, main_bs;
	XtVaGetValues(mainForm, XmNtopShadowColor, &main_ts, NULL);
	XtVaGetValues(mainForm, XmNbottomShadowColor, &main_bs, NULL);
	
	/* pull down form to hold the sub-entries for the File menu */
	Widget fileMenuPane = XmCreatePulldownMenu(menuBar, "fileMenuPane", NULL, 0);
	XtVaSetValues(fileMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	/* XmCascadeButton for the visible File entry on the menuBar
	   It posts the drop down fileMenuPane when activated  */
	Widget fileMenu = XtVaCreateManagedWidget("File", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, fileMenuPane,
		XmNmnemonic, 'F',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
		XmNmarginHeight, 2,
		XmNtopOffset, 20,
	NULL);
	
	Widget fileQuit = XtVaCreateManagedWidget("fileQuit", xmPushButtonWidgetClass, fileMenuPane,
		XmNmnemonic, 'Q',
		XmNmarginWidth, 7,
		XmNmarginHeight, 5,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>Q", 10,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + Q ", 10,
		XtVaTypedArg, XmNlabelString, XmRString, "Quit", 5,
	NULL);
	XtAddCallback(fileQuit, XmNactivateCallback, instantQuit, NULL);





/* Outputs menu */
	Widget outMenuPane = XmCreatePulldownMenu(menuBar, "outMenuPane", NULL, 0);
	XtVaSetValues(outMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	/* XmCascadeButton for the visible File entry on the menuBar
	   It posts the drop down fileMenuPane when activated  */
	Widget outMenu = XtVaCreateManagedWidget("Options", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, outMenuPane,
		XmNmnemonic, 'O',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
		XmNmarginHeight, 2,
		XmNtopOffset, 20,
	NULL);

	Widget outReset = XtVaCreateManagedWidget("outReset", xmPushButtonWidgetClass, outMenuPane,
		XmNmnemonic, 'R',
		XmNmarginWidth, 7,
		XmNmarginHeight, 5,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>R", 10,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + R ", 10,
		XtVaTypedArg, XmNlabelString, XmRString, "Reset", 6,
	NULL);
	XtAddCallback(outReset, XmNactivateCallback, resetDefaults, NULL);



/* Window menu */
	Widget winMenuPane = XmCreatePulldownMenu(menuBar, "winMenuPane", NULL, 0);
	XtVaSetValues(winMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	/* XmCascadeButton for the visible File entry on the menuBar
	   It posts the drop down fileMenuPane when activated  */
	Widget winMenu = XtVaCreateManagedWidget("Window", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, winMenuPane,
		XmNmnemonic, 'W',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
		XmNmarginHeight, 2,
		XmNtopOffset, 20,
	NULL);
	
	Widget winIconify = XtVaCreateManagedWidget("winIconify", xmPushButtonWidgetClass, winMenuPane,
		XmNmnemonic, 'I',
		XmNmarginWidth, 7,
		XmNmarginHeight, 5,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>I", 10,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + I ", 10,
		XtVaTypedArg, XmNlabelString, XmRString, "Iconify", 8,
	NULL);
	XtAddCallback(winIconify, XmNactivateCallback, iconifyCallback, NULL);
	
	
/* Help menus */
	Widget helpMenuPane = XmCreatePulldownMenu(menuBar, "helpMenuPane", NULL, 0);
	XtVaSetValues(helpMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	Widget helpMenu = XtVaCreateManagedWidget("Help", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, helpMenuPane,
		XmNmnemonic, 'H',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	Widget helpEntry0 = XtVaCreateManagedWidget("helpEntry0", xmPushButtonWidgetClass, helpMenuPane,
		XmNmnemonic, 'A',
		XmNmarginWidth, 7,
		XmNmarginHeight, 5,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		
		XtVaTypedArg, XmNlabelString, XmRString, "About", 6,
	NULL);
	XtAddCallback(helpEntry0, XmNactivateCallback, aboutButtonCallback, (XtPointer)topLevel);
	
	Widget helpEntry1 = XtVaCreateManagedWidget("helpEntry1", xmPushButtonWidgetClass, helpMenuPane,
		XmNmnemonic, 'L',
		XmNmarginWidth, 7,
		XmNmarginHeight, 5,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		
		XtVaTypedArg, XmNlabelString, XmRString, "License", 8,
	NULL);
	XtAddCallback(helpEntry1, XmNactivateCallback, licenseButtonCallback, (XtPointer)topLevel);
	

/* let the menu bar know we want help on the right */
	XtVaSetValues(menuBar,
		XmNmenuHelpWidget, helpMenu,
	NULL);
	
	

/* create master slider */
	masterScale = XtVaCreateManagedWidget("masterScale", xmScaleWidgetClass, mainForm,
		XmNorientation, XmHORIZONTAL,
		XmNminimum, 0,
		XmNmaximum, 100,
		XmNvalue, masterVolume,
		XmNshowValue, 0,
		
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, menuBar,
		XmNtopOffset, 10,
		
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 6,
		
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 6,
		
		
		XmNsliderMark, XmETCHED_LINE,
		XmNhighlightThickness, 0,
		
		
	NULL);
	/* make sure ticks stay after the xtvasetvalue function */
	/* or else it won't pick up the orientation */
	XmScaleSetTicks(masterScale, 100, 9, 1, 12, 9, 6);

	/* callback for master scale widget */
	XtAddCallback(masterScale, XmNdragCallback, draggingMasterScale, NULL);
	XtAddCallback(masterScale, XmNvalueChangedCallback, valueChangedMasterScale, NULL);
	
	
	Widget colorFrame0 = XtVaCreateManagedWidget("colorFrame0", xmFrameWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, masterScale,
		XmNtopOffset, 0,
		
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 6,
		
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 6,
		
		
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_IN,
	NULL);
	
	createColorBars(colorFrame0, 6);
	
	
	
	masterLabel = XtVaCreateManagedWidget("masterLabel", xmLabelWidgetClass, mainForm,
		XmNalignment, XmALIGNMENT_CENTER,
		XtVaTypedArg, XmNlabelString, XmRString, "Master", 4,
		
		XmNmarginWidth, 0,
		XmNmarginHeight, 1,
		
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, colorFrame0,
		XmNtopOffset, 5,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 12,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 7,
	NULL);
	
/* set percentage label */
	char perc[64];
	snprintf(perc, sizeof(perc), "[ %d%% ]", masterVolume);
	
	percLabel = XtVaCreateManagedWidget("percLabel", xmTextWidgetClass, mainForm,
		
		/* XtVaTypedArg, XmNvalue, XmRString, perc, sizeof(perc), */
		
		XmNvalue, perc,
		XmNforeground, menu_fg,
		XmNbackground, menu_text,
		
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, colorFrame0,
		XmNtopOffset, 7,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, masterLabel,
		XmNleftOffset, 1,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 7,
		
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNeditMode, XmSINGLE_LINE_EDIT,
		XmNeditable, 0,
		XmNscrollVertical, 0,
		XmNscrollHorizontal, 0,
		XmNcursorPositionVisible, 0,
		XmNautoShowCursorPosition, 0,
		XmNtraversalOn, 0,
		XmNshadowThickness, 0,
		XmNmarginWidth, 5,
		XmNwidth, 90,
	NULL);
	
/*reset button pixmap */
	Pixmap reset_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), reset_bits, reset_width, reset_height);
	
/* create reset button */
	Widget resetButton = XmCreatePushButton(mainForm, "resetButton", NULL, 0);
	XtVaSetValues(resetButton,
		XmNhighlightThickness, 0,
		XmNshadowThickness, 2,
		XmNlabelType, XmPIXMAP,
		/* XtVaTypedArg, XmNlabelString, XmRString, "Reset", 4, */
		XmNmarginLeft, 6,
		XmNmarginRight, 6,
		XmNspacing, 0,
		XmNheight, 34,
		XmNminHeight, 34,
		
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, colorFrame0,
		XmNtopOffset, 5,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 7,
		XmNarmPixmap, reset_pixmap,
	NULL);
	XtManageChild(resetButton);

	/* when reset button is pushed reset master scale */
	XtAddCallback(resetButton, XmNactivateCallback, resetDefaults, masterScale);


/* get colors from reset button */
	Pixel reset_fg, reset_bg, reset_ts, reset_bs;

	XtVaGetValues(resetButton,
		XmNforeground, &reset_fg, /* foreground */
		XmNbackground, &reset_bg, /* background */
		XmNtopShadowColor, &reset_ts, /* top shadow */
		XmNbottomShadowColor, &reset_bs, /* bottom shadow */
	NULL);


/* unmuted pixmap */
	Pixmap mute0_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), mute0_bits, mute0_width, mute0_height);
	
/* muted pixmap */
	Pixmap mute1_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), mute1_bits, mute1_width, mute1_height);

/* create master mute toggle in lower right corner */
	toggleButton = XmCreateToggleButton(mainForm, "toggleButton", NULL, 0);
	XtVaSetValues(toggleButton,
		XmNforeground, reset_fg,
		XmNbackground, reset_bg,	/* resetButton colors are applied to */
		XmNtopShadowColor, reset_ts,	/* toggleButton for visual consistency */
		XmNbottomShadowColor, reset_bs,
		XmNhighlightThickness, 0,
		XmNshadowThickness, 2,
		XmNenableToggleVisual, 0,
		XmNlabelType, XmPIXMAP,
		XmNindicatorOn, XmNONE,
		XmNspacing, 0,
		XmNheight, 34,
		XmNminHeight, 34,
		
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, colorFrame0,
		XmNtopOffset, 5,
		
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 7,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 7,
		XmNlabelPixmap, mute0_pixmap,
		XmNselectPixmap, mute1_pixmap,
		XmNmarginLeft, 6,
		XmNmarginRight, 4,
	NULL);
	XtManageChild(toggleButton);

	/* callback for mute button */
	XtAddCallback(toggleButton, XmNvalueChangedCallback, toggleMasterMute, NULL);


	/* attachment point and offset are set down here after toggleButton creation */
	XtVaSetValues(resetButton,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, toggleButton,
		XmNrightOffset, 4,
	NULL);
	
	
	XtAppAddTimeOut(app, widgetUpdate, refreshWidgets, NULL);
	XtAppAddTimeOut(app, outputUpdate, fetchCurrentAudioOutput, NULL);
	
	
	/* close mainForm and topLevels */
	XtManageChild(mainForm);
	XtRealizeWidget(topLevel);

	/* keyboard input focus defaults to volume slider */
	XmProcessTraversal(masterScale, XmTRAVERSE_CURRENT);
	
	/* enter processing loop */
	XtAppMainLoop(app);
	return NULL;
}


void createUserInterface(int argc, char *argv[])
{
/* allocate and initialize thread arguments */
	MotifThreadArgs *args = (MotifThreadArgs *)malloc(sizeof(MotifThreadArgs));
	args->argc = argc;
	args->argv = argv;
	
/* spawn thread to run main Motif GUI loop */
	if(pthread_create(&motifGUI, NULL, spawnMotifGUI, (void *)args) != 0)
	{
		fprintf(stderr, "\n   createUserInterface: pthread_create could not create Motif GUI thread.\n\n");
		fflush(stderr);
		free(args);
		exit(1);
	}
}
