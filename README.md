### Audio Control

Simple audio control panel for X11 with a Motif GUI. It's a wrapper around ALSA/amixer and PulseAudio/pacmd.


![screenshot](screenshots/dvaudio_sample.png)




![screenshot](screenshots/dvaudio_sample2.png)

#### Features

- Adjust and mute master volume
- Controllable with POSIX signals

#### Requirements 

- Motif 2.3.8
- X11/Xlib
- libXpm
- pthreads
- amixer
- pacmd

#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
./dvaudio
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```


#### License

This software is distributed free of charge under the BSD Zero Clause license.