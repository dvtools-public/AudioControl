/* callbacks for audio panel */

void instantQuit() { exit(0); }


int fetchCurrentVolume()
{
	int volume = -1;
	
	/* open pipe with given command */
	FILE* audioCmd = popen(currentVolumeCmdStr, "r");
	if(audioCmd == NULL)
	{
		fprintf(stdout, "\n   Audio Control -> fetchCurrentVolume(): ");
		fprintf(stdout, "\n   Error: Could not run command. \n\n");
		fflush(stdout);
		
		volume = -1;
	}
	
	/* throw error if it fails */
	if(fscanf(audioCmd, "%d", &volume) == 0)
	{
		fprintf(stdout, "\n   Audio Control -> fetchCurrentVolume(): ");
		fprintf(stdout, "\n   Error: Could not read value. \n\n");
		fflush(stdout);
		
		volume = -1;
	}
	
	/* close cmd pipe */
	pclose(audioCmd);
	
	/* return zero or the new number from the cmd */
	return volume;
}

void fetchCurrentAudioOutput(XtPointer clientData, XtIntervalId *id)
{
	
	FILE* audioOutput = popen("pacmd list-sinks", "r");
	if(audioOutput == NULL)
	{
		fprintf(stdout, "\n   Audio Control -> fetchCurrentAudioOutput(): ");
		fprintf(stdout, "\n   Error: Could not run command. \n\n");
		fflush(stdout);
		
		return;
	}
	
	char line[2048];
	while(fgets(line, sizeof(line), audioOutput) != NULL)
	{
		/* if any line contains active port... */
		if(strstr(line, "active port") != NULL)
		{
		/* removes unwanted chars */
			char activePort[512];
			sscanf(line, "%*[^:]: %[^\n]", activePort);
			
		/* keep track of previous type */
			int previousOutput = outputType;
				
			char *newLabel = NULL;
			
		/* if activePort line is... */
			if(strstr(activePort, "speaker") != NULL)
			{
				/* set global output type int */
				outputType = 1;
				
				/* set output type string and apply to label widget */
				newLabel = "Speakers";
			}
			
			else if(strstr(activePort, "headphones") != NULL) 
			{
				outputType = 2; 
				newLabel = "Headphones";
			}
			
			else
			{
				outputType = 0;
				newLabel = "Master";
			}
				
			if(outputType != previousOutput)
			{
			/* fetch master volume level again */
				masterVolume = fetchCurrentVolume();
                    		
			/* set the master scale value to the new volume percentage */
				XmScaleSetValue(masterScale, masterVolume);
				
			/* unmute master volume */
				system("amixer set Master unmute > /dev/null 2>&1");
				
			/* untoggle the button */
				XmToggleButtonSetState(toggleButton, False, False);
				
			/* unlock master scale when unmuted */
				XtVaSetValues(masterScale,
					XmNsensitive, 1,
				NULL);
				
				if(newLabel != NULL)
				{
					XtVaSetValues(masterLabel, XmNlabelString, 0, NULL);
					
					XtVaSetValues(masterLabel,
						XtVaTypedArg, XmNlabelString, XmRString, 
						newLabel, strlen(newLabel) + 1,
					NULL);
				}
				
			}
		}
		
	}
	
	/* close file */
	pclose(audioOutput);
	
	/* loop this timer */
	XtAppAddTimeOut(app, outputUpdate, fetchCurrentAudioOutput, clientData);
}

/* using Xt timers is less messy than trying to update the GUI from other threads */
void refreshWidgets(XtPointer clientData, XtIntervalId *id)
{
	/* hold off on scale updates while in motion */
	if(scaleIsDragging == 0) { XmUpdateDisplay(masterScale); }
	
	char perc[64];
	snprintf(perc, sizeof(perc), "[ %d%% ]", masterVolume);
	XmTextSetString(percLabel, perc);
	
	/* loop this timer */
	XtAppAddTimeOut(app, widgetUpdate, refreshWidgets, clientData);
}





/* used for XmNvalueChangedCallback when the mouse is released */
void valueChangedMasterScale()
{
	/* fetch current value from masterScale */
	XtVaGetValues(masterScale, XmNvalue, &masterVolume, NULL);
	
	scaleIsDragging = 0;
	
	char cmd[64];
	/* use alsa mixer to set volume percentage and gets rid of amixer outputs */
	sprintf(cmd, setMasterCmdStr, masterVolume);
	system(cmd);
}


/* used for XmNdragCallback to detect motion */
void draggingMasterScale()
{
	scaleIsDragging = 1;
	
	XtVaGetValues(masterScale, XmNvalue, &masterVolume, NULL);
}


void toggleMasterMute(Widget widget, XtPointer client_data, XtPointer call_data)
{
	XmToggleButtonCallbackStruct *cbs = (XmToggleButtonCallbackStruct *)call_data;
	
	if(cbs->set) /* mute */
        {
		/* mute master volume */
        	system(muteMasterCmdStr);
		
		/* lock and grey out master scale while muted */
		XtVaSetValues(masterScale, XmNsensitive, 0, NULL);
	}
	
	else /* unmute */
	{
        	/* unmute master volume */
        	system(unmuteMasterCmdStr);
		
		/* unlock master scale when unmuted */
		XtVaSetValues(masterScale, XmNsensitive, 1, NULL);
	}
}


void resetDefaults()
{
	/* make sure scale widget is active and unlocked */
	XtVaSetValues(masterScale, XmNsensitive, 1, NULL);
	
	/* reset master volume and scale to 40 percent */
	XmScaleSetValue(masterScale, 40);
	char cmd[64];
	sprintf(cmd, setMasterCmdStr, 40);
	system(cmd);
	
	/* make sure we aren't muted */
	XmToggleButtonSetState(toggleButton, False, False);
	system(unmuteMasterCmdStr);
	
	masterVolume = fetchCurrentVolume();
}


void userSignal1()
{
	/* subtract 1 from current volume */
	int newVolume = masterVolume - 1;
	
	if(newVolume < 0) { return; }
	else { masterVolume = newVolume; }
	
	/* set the master scale value to the new volume percentage */
	XmScaleSetValue(masterScale, newVolume);
	
	/* set the system volume through amixer */
	char cmd[50];
	sprintf(cmd, setMasterCmdStr, newVolume);
	system(cmd);
}


void userSignal2()
{
	int newVolume = masterVolume + 1;
	
	if(newVolume > 100) { return; }
	else { masterVolume = newVolume; }
	
	/* set the master scale value to the new volume percentage */
	XmScaleSetValue(masterScale, newVolume);
	
	/* set the system volume through amixer */
	char cmd[50];
	sprintf(cmd, setMasterCmdStr, newVolume);
	system(cmd);
}


void userSignal3() /* remapped SIGALRM */
{
	Boolean toggleState = XmToggleButtonGetState(toggleButton);
	XmToggleButtonSetState(toggleButton, !toggleState, True);
}


void iconifyCallback()
{
	XtVaSetValues(topLevel,
		XmNiconic, 1, /* tells topLevel shell to minimize */
	NULL);
}


void aboutButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
	char notifierStrings[1024];
	snprintf(notifierStrings, sizeof(notifierStrings), 
	"%s%s%s%s%s%s%s",
	
		"DeskView Audio Control Panel \n",
		"Version: ", versionString, "\n",
		"Date: ", dateString, "\n",
	NULL);
	
	createInfoDialog(callingWidget, topLevel, "About", notifierStrings, 1, 1, 2);
}


void licenseButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
	char notifierStrings[1024];
	snprintf(notifierStrings, sizeof(notifierStrings), 
	"%s%s",
	
		"This program is distributed free of charge \n",
		"under the BSD Zero Clause license. \n",
	NULL);
	
	createInfoDialog(callingWidget, topLevel, "License", notifierStrings, 0, 1, 2);
}


